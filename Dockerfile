FROM node

WORKDIR /app

COPY package.json /app

RUN npm install 

COPY . /app

EXPOSE 8000

CMD ["npm","start"]

# FROM node AS ui-build
# WORKDIR /usr/src/app
# COPY client/ ./client/
# RUN cd client && npm install && npm run build

# FROM node AS server-build
# WORKDIR /app
# COPY --from=ui-build /usr/src/app/client/dist /app/client/dist
# COPY package.json /app
# RUN npm install
# COPY . /app

# EXPOSE 8000

# CMD ["npm","start"]